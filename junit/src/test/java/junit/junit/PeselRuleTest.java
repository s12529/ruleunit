package junit.junit;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.PeselRule;
import domain.Person;

public class PeselRuleTest {
	
	PeselRule rule = new PeselRule();

	@Test
	public void check_pesel_is_not_null(){
		Person p = new Person();
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.ERROR));	
	}
	
	@Test
	public void check_pesel_is_not_empty(){
		Person p = new Person();
		p.setPesel("");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.ERROR));
		
	}
	
	@Test
	public void check_pesel_is_not_correct(){
		Person p = new Person();
		p.setPesel("12345");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.ERROR));
		
	}
	
	@Test
	public void check_pesel_is_correct(){
		Person p = new Person();
		p.setPesel("11111111111");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.OK));
		
	}
	
	@Test
	public void check_pesel_is_correct_to_date_of_birth(){
		Person p = new Person();
		Calendar dob = new GregorianCalendar(1993,9,21);
		p.setDateOfBirth(dob);
		p.setPesel("93102111111");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.OK));
		
	}

}
