package junit.junit;

import static org.junit.Assert.*;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.PasswordRule;
import domain.Person;
import domain.User;

public class PasswordRuleTest {

	PasswordRule rule = new PasswordRule();
	
	@Test
	public void check_password_is_strong(){
		Person p = new Person();
		User u = new User();
		u.setPassword("Razdwa");
		p.setUser(u);
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.OK));
		
	}
	
	@Test
	public void check_password_is_not_strong(){
		Person p = new Person();
		User u = new User();
		u.setPassword("razdwa");
		p.setUser(u);
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.ERROR));
		
	}

}
