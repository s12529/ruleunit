package junit.junit;

import static org.junit.Assert.*;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.NipRule;
import domain.Person;

public class NipRuleTest {

	NipRule rule = new NipRule();
	
	@Test
	public void check_nip_is_correct(){
		Person p = new Person();
		p.setNip("3623981230");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.OK));
		
	}
	
	@Test
	public void check_nip_is_not_correct(){
		Person p = new Person();
		p.setNip("11111111111");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.ERROR));
		
	}
}
