package junit.junit;

import static org.junit.Assert.*;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.EmailRule;
import domain.Person;

public class EmailRuleTest {

	EmailRule rule = new EmailRule();
	
	@Test
	public void check_email_is_correct(){
		Person p = new Person();
		p.setEmail("abc@abc.com");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.OK));
		
	}
	
	@Test
	public void check_email_is_not_correct(){
		Person p = new Person();
		p.setPesel("11111111111");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.ERROR));
		
	}

}
