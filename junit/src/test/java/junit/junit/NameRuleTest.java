package junit.junit;

import static org.junit.Assert.*;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.NameRule;
import domain.Person;

public class NameRuleTest {

	NameRule rule = new NameRule();
	
	@Test
	public void check_name_is_not_null(){
		Person p = new Person();
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.ERROR));	
	}
	
	@Test
	public void check_name_is_not_empty(){
		Person p = new Person();
		p.setFirstName("");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.ERROR));
		
	}
	
	@Test
	public void check_name_and_return_ok_if_not_null(){
		Person p = new Person();
		p.setFirstName("Ktos");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.OK));
		
	}
	
	

}
