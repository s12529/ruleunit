package checker.rules;

import checker.CheckResult;
import checker.ICheckRule;
import checker.RuleResult;
import domain.Person;

public class PasswordRule implements ICheckRule<Person> {

	public CheckResult checkRule(Person entity) {
		
		if(entity.getUser().getPassword() == null) {
			return new CheckResult("Brak hasla.", RuleResult.ERROR);
		}
		
		if(entity.getUser().getPassword().equals("")) {
			return new CheckResult("Brak hasla.", RuleResult.ERROR);
		}
		
		if(entity.getUser().getPassword().length() < 5) {
			return new CheckResult("Haslo jest za krotkie", RuleResult.ERROR);
		}
		
		if(entity.getUser().getPassword().equals(entity.getUser().getPassword().toLowerCase())) {
			return new CheckResult("Haslo musi zawierac conajmniej jedna duza litere.", RuleResult.ERROR);
		}
		
		return new CheckResult("", RuleResult.OK);
	}

}
