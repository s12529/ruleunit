package checker.rules;

import checker.CheckResult;
import checker.ICheckRule;
import checker.RuleResult;
import domain.Person;

public class DateOfBirthRule implements ICheckRule<Person> {

	public CheckResult checkRule(Person entity) {
		
		if(entity.getDateOfBirth() == null) {
			return new CheckResult("Brak daty urodzin.", RuleResult.ERROR);
		}
		
		if(entity.getDateOfBirth().equals("")) {
			return new CheckResult("Brak daty urodzin.", RuleResult.ERROR);
		}
		
		return new CheckResult("", RuleResult.OK);
	}

}
