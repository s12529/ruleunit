package checker.rules;

import checker.CheckResult;
import checker.ICheckRule;
import checker.RuleResult;
import domain.Person;
import org.apache.commons.validator.routines.EmailValidator;

public class EmailRule implements ICheckRule<Person> {

	public CheckResult checkRule(Person entity) {
		
		if(entity.getEmail() == null) {
			return new CheckResult("Brak email.", RuleResult.ERROR);
		}
		
		if(entity.getEmail().equals("")) {
			return new CheckResult("Brak email.", RuleResult.ERROR);
		}
		
		if(!EmailValidator.getInstance().isValid(entity.getEmail()))
		{
			return new CheckResult("Niepoprawny format email.", RuleResult.ERROR);
		}
		
		
		return new CheckResult("", RuleResult.OK);
	}

}
