package checker.rules;

import checker.CheckResult;
import checker.ICheckRule;
import checker.RuleResult;
import domain.Person;

public class NipRule implements ICheckRule<Person> {

	public CheckResult checkRule(Person entity) {
		
		if(entity.getNip() == null) {
			return new CheckResult("Brak NIP.", RuleResult.ERROR);
		}
		
		if(entity.getNip().equals("")) {
			return new CheckResult("Brak NIP.", RuleResult.ERROR);
		}
		
			
		if(entity.getNip().length() != 10) {
			return new CheckResult("Brak NIP.", RuleResult.ERROR);
		}
		
		
		if(!checkNipControlSum(entity)) {
			return new CheckResult("Bledny numer NIP.", RuleResult.ERROR);
		}
		
	
		return new CheckResult("", RuleResult.OK);
	}
	
	
	
	private boolean checkNipControlSum(Person entity) {
		
		int ScaleTab[] = {6, 5, 7, 2, 3, 4, 5, 6, 7};
		int sum = 0;
		
		for(int i = 0; i < ScaleTab.length; i++) {
			
			sum += Integer.parseInt(entity.getNip().substring(i, i + 1)) * ScaleTab[i];
			
		}
		
		if(!entity.getNip().substring(9).equals(Integer.toString(sum % 11))) {	
			return false;		
		}
	
		return true;	
	}

}
