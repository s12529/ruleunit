package checker.rules;

import checker.CheckResult;
import checker.ICheckRule;
import checker.RuleResult;
import domain.Person;

public class NameRule implements ICheckRule<Person>{

	public CheckResult checkRule(Person entity) {
		
		if(entity.getFirstName() == null) {
			return new CheckResult("Brak imienia.", RuleResult.ERROR);
		}
		
		if(entity.getFirstName().equals("")) {
			return new CheckResult("Brak imienia.", RuleResult.ERROR);
		}
		
		
		return new CheckResult("", RuleResult.OK);
	}

}
