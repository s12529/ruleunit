package checker.rules;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import checker.CheckResult;
import checker.ICheckRule;
import checker.RuleResult;
import domain.Person;

public class PeselRule implements ICheckRule<Person> {

	public CheckResult checkRule(Person entity) {
		
		if(entity.getPesel() == null) {
			return new CheckResult("Brak nr PESEL.", RuleResult.ERROR);
		}
		
		if(entity.getPesel().equals("")) {
			return new CheckResult("Brak nr PESEL.", RuleResult.ERROR);
		}
		
		if(entity.getPesel().length() != 11 ) {
			return new CheckResult("Nieprawidlowy format PESEL", RuleResult.ERROR);
		}
		
		if(entity.getDateOfBirth() != null) {
		DateFormat df = new SimpleDateFormat("YYMMdd");
		String reportDf = df.format(entity.getDateOfBirth().getTime());
		
			if(!reportDf.equals(entity.getPesel().substring(0, 6)))
			{
				return new CheckResult("Nr PESEL nie zgadza sie z data urodzenia.", RuleResult.ERROR);
			}
		}
		
		return new CheckResult("", RuleResult.OK);
	}

}
