package checker;

public interface ICheckRule <TEntity> {

	CheckResult checkRule(TEntity entity);
	
}
