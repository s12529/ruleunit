package checker;

import java.util.ArrayList;
import java.util.List;

public class RuleChecker<TEntity> {
	
	private List<ICheckRule<TEntity>> rules;
	
	public RuleChecker() {
		rules = new ArrayList<ICheckRule<TEntity>>();
	}
	
	public List<ICheckRule<TEntity>> getRules() {
		return rules;
	}

	public void setRules(List<ICheckRule<TEntity>> rules) {
		this.rules = rules;
	}

	public List<CheckResult> check(TEntity entity) {
		
		List<CheckResult> result  = new ArrayList<CheckResult>();
		
		for(ICheckRule<TEntity> rule: rules){
			result.add(rule.checkRule(entity));
		}
		return result;
	}
	

}
