package domain;

import java.util.ArrayList;
import java.util.List;

public class RolesPermission extends Entity {

	private int roleId;
	private int permissionId;
	private List<UserRoles> roles;

	public RolesPermission()
	{
		setRoles(new ArrayList<UserRoles>());
	}
	
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	
	public int getPermissionId() {
		return permissionId;
	}
	public void setPermissionId(int permissionId) {
		this.permissionId = permissionId;
	}

	public List<UserRoles> getRoles() {
		return roles;
	}

	public void setRoles(List<UserRoles> roles) {
		this.roles = roles;
	}
	
	public void addRole(UserRoles role) {
		roles.add(role);
	}
	
	
}
