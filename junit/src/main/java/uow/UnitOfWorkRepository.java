package uow;

import domain.Entity;

public interface UnitOfWorkRepository {
	
	void persistAdd(Entity entity);
    void persistRemove(Entity entity);
    void persistUpdate(Entity entity);

}
