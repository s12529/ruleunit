package repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import domain.EntityState;
import repository.Database;
import domain.Entity;
import domain.EnumerationValue;
import repository.EnumerationValueRepository;
import uow.UnitOfWorkRepository;

public class HsqlEnumerationValuesRepository implements EnumerationValueRepository, UnitOfWorkRepository {
	
Database db = new Database();
	
	protected PreparedStatement insert;
	protected PreparedStatement delete;
	protected PreparedStatement update;

	public EnumerationValue withId(int id) {
		EnumerationValue enumerationValue = null;
		
		for(EnumerationValue v: db.enumValues)
		{
			if (v.getId() == id) {
				enumerationValue = v;
			}
		}
		return enumerationValue;
	}

	public List<EnumerationValue> allOnPage(PagingInfo page) {
		List<EnumerationValue> enumerationValues = new ArrayList<EnumerationValue>();
		
		for(EnumerationValue v: db.enumValues)
		{
			
			if(page.getTotalCount() <= page.getPageSize()) {
				enumerationValues.add(v);
				page.setTotalCount(page.getTotalCount()+1);
			}
		}
		
		page.setPage(page.getPage()+1);
		
		return enumerationValues;
	}

	public void add(EnumerationValue entity) {
		entity.setState(EntityState.NEW);
		db.enumValues.add(entity);
	}

	public void delete(EnumerationValue entity) {
		entity.setState(EntityState.DELETED);
		db.enumValues.remove(entity);
	}

	public void modify(EnumerationValue entity) {
		entity.setState(EntityState.MODIFIED);
		@SuppressWarnings("unused")
		EnumerationValue enumerationValue = null;
		
		for(EnumerationValue v: db.enumValues)
		{
			if(v.getId() == entity.getId()) {
				enumerationValue = v;
			}
		}
		
		// jakies operacje
			
	}

	public int count() {
		return db.enumValues.size();	
	}

	public void persistAdd(Entity entity) {
		try {
			setUpInsertQuery((EnumerationValue)entity);
			insert.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void persistRemove(Entity entity) {
		try {
			delete.setInt(1, entity.getId());
			delete.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void persistUpdate(Entity entity) {
		try {
			setUpUpdateQuery((EnumerationValue)entity);
			update.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public EnumerationValue withName(String name) {
		EnumerationValue enumerationValue = null;
		
		for(EnumerationValue v: db.enumValues)
		{
			if (v.getEnumerationName() == name) {
				enumerationValue = v;
			}
		}
		return enumerationValue;
	}

	public EnumerationValue withKey(int key, String name) {

		EnumerationValue enumerationValue = null;
		
		for(EnumerationValue v: db.enumValues)
		{
			if (v.getIntKey() == key && v.getEnumerationName() == name) {
				enumerationValue = v;
			}
		}
		return enumerationValue;
	}

	public EnumerationValue withStringKey(String key, String name) {

		EnumerationValue enumerationValue = null;
		
		for(EnumerationValue v: db.enumValues)
		{
			if (v.getStringKey() == key && v.getEnumerationName() == name) {
				enumerationValue = v;
			}
		}
		return enumerationValue;
	}
	
	protected void setUpUpdateQuery(EnumerationValue entity) throws SQLException { //sql
	}
	protected void setUpInsertQuery(EnumerationValue entity) throws SQLException { //sql
	}

}
