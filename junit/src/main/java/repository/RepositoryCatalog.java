package repository;

import domain.EnumerationValue;
import domain.User;

public interface RepositoryCatalog {
	
	public Repository<EnumerationValue> enumerations();
	public Repository<User> users();
	

}
