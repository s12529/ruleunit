package repository;

import domain.EnumerationValue;
import domain.User;

public class HsqlRepositoryCatalog implements RepositoryCatalog {

	
	private Database db = new Database();
	
	public Repository<EnumerationValue> enumerations() {
		return (EnumerationValueRepository) db.enumValues;
	}

	public Repository<User> users() {
		return (UserRepository) db.users;
	}

}
