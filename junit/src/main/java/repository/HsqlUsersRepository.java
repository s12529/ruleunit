package repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import uow.UnitOfWorkRepository;
import domain.Entity;
import domain.EntityState;
import domain.RolesPermission;
import domain.User;
import domain.UserRoles;

public class HsqlUsersRepository implements UserRepository, UnitOfWorkRepository {
	
	protected PreparedStatement insert;
	protected PreparedStatement delete;
	protected PreparedStatement update;
	
	Database db = new Database();


	
	protected void setUpUpdateQuery(User entity) throws SQLException { //sql
	}
	protected void setUpInsertQuery(User entity) throws SQLException { //sql
	}
	
	
	public User withId(int id) {
		User user = null;
		
		for(User v: db.users)
		{
			if (v.getId() == id) {
				user = v;
			}
		}
		return user;
	}
	public List<User> allOnPage(PagingInfo page) {
		List<User> user = new ArrayList<User>();
		
		for(User v: db.users)
		{
			
			if(page.getTotalCount() <= page.getPageSize()) {
				user.add(v);
				page.setTotalCount(page.getTotalCount()+1);
			}
		}
		
		page.setPage(page.getPage()+1);
		
		return user;
	}
	public void add(User entity) {
		entity.setState(EntityState.NEW);
		db.users.add(entity);
		
	}
	public void delete(User entity) {
		entity.setState(EntityState.DELETED);
		db.users.remove(entity);
		
	}
	public void modify(User entity) {
		entity.setState(EntityState.MODIFIED);
		@SuppressWarnings("unused")
		User user = null;
		
		for(User v: db.users)
		{
			if(v.getId() == entity.getId()) {
				user = v;
			}
		}
		// jakies operacje
	}
	public int count() {
		return db.users.size();
	}
	public void persistAdd(Entity entity) {
		// TODO Auto-generated method stub
		
	}
	public void persistRemove(Entity entity) {
		// TODO Auto-generated method stub
		
	}
	public void persistUpdate(Entity entity) {
		// TODO Auto-generated method stub
		
	}
	public User withLogin(String login) {
		User user = null;
		
		for(User v: db.users)
		{
			if (v.getLogin() == login) {
				user = v;
			}
		}
		return user;
	}
	public User withLoginAndPassword(String login, String password) {
		User user = null;
		
		for(User v: db.users)
		{
			if (v.getLogin() == login && v.getPassword() == password) {
				user = v;
			}
		}
		return user;
	}
	public void setupPermission(User user) {
		UserRoles role = new UserRoles();
		RolesPermission rolesPermission = new RolesPermission();
		rolesPermission.setPermissionId(1); // dodaj, itp
		rolesPermission.setRoleId(1); // admin, itp
		role.addRolePermission(rolesPermission);
		role.addUser(user);
		role.setUserId(0);
		role.setRoleId(rolesPermission.getRoleId());
		user.addRole(role);
		rolesPermission.addRole(role);
		
	}

}
