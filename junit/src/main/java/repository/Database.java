package repository;

import java.util.ArrayList;
import java.util.List;

import domain.EnumerationValue;
import domain.RolesPermission;
import domain.User;
import domain.UserRoles;

public class Database {

	List<User> users;
	List<RolesPermission> userPermission;
	List<UserRoles> userRoles;
	List<EnumerationValue> enumValues;
	
	
	public Database() {
		
		users = new ArrayList<User>();
		userPermission = new ArrayList<RolesPermission>();
		userRoles = new ArrayList<UserRoles>();
		enumValues = new ArrayList<EnumerationValue>();
		
	}
	
	
}
